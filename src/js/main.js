$(function () {
    //dom input getting
    let $searchBar = $('#searchBar'),
        $searchForm = $('#searchForm'),
        $filmList = $('#filmList'),
        $tvShowsList = $('#tvShowsList'),
        $artistList = $('#artistList'),
        $topList = $('#topList');

    //ajax request on form submit

    $searchForm.submit(function (event) {
        event.preventDefault();
        //emptying older lists
        $filmList.html('');
        $tvShowsList.html('');
        $artistList.html('');
        $topList.html('');
        //search in movies
        $.get(`https://api.themoviedb.org/3/search/movie?api_key=4bf086d1df9acf40d8bb81636c65a8c8&language=fr&query=${$searchBar.val()}&page=1&include_adult=false`, function (response) {
            if (!response.results) {
                return;
            }
            for (const movie of response.results) {
                if (!movie.poster_path) {
                    let tpl =
                        `
                <li class="row">
                    <div class="filmTxt col-12">
                        <a id="${movie.id}" href='' >${movie.title} + d'infos</a>
                        <p>
                        <span>Date de sortie : ${movie.first_air_date}</span>
                        ${(movie.overview).slice(0, 200)} ...
                        </p>
                    </div>
                </li>
              `
                    $filmList.append(tpl);
                }
                else {
                    let tpl =
                        `
                <li class="row">
                    <div class="col-3">
                        <img class="filmPict" src="https://image.tmdb.org/t/p/w500/${movie.poster_path}">
                    </div>
                    <div class="filmTxt col-11 col-md-9">
                        <a id="${movie.id}" href='' >${movie.title} + d'infos</a>
                        <p>
                        <span>Date de sortie : ${movie.release_date}</span>
                        ${(movie.overview).slice(0, 200)} ...
                        </p>
                    </div>
                </li>
              `
                    $filmList.append(tpl);
                }
            }
        })
        //search in tv show
        $.get(`https://api.themoviedb.org/3/search/tv?api_key=4bf086d1df9acf40d8bb81636c65a8c8&language=fr&query=${$searchBar.val()}&page=1`, function (response) {
            if (!response.results) {
                return;
            }
            for (const serie of response.results) {
                if (!serie.poster_path) {
                    let tpl =
                        `
                <li class="row">
                    <div class="filmTxt col-12">
                        <a id="${serie.id}" href='' >${serie.name} + d'infos</a>
                        <p>
                        <span>Date de sortie : ${serie.first_air_date}</span>
                        ${serie.overview}
                        </p>
                    </div>
                </li>
              `
                    $tvShowsList.append(tpl);
                }
                else {
                    let tpl =
                        `
                <li class="row">
                    <div class="col-3">
                        <img class="filmPict" src="https://image.tmdb.org/t/p/w500/${serie.poster_path}">
                    </div>
                    <div class="filmTxt col-11 col-md-9">
                        <a id="${serie.id}" href='' >${serie.name} + d'infos</a>
                        <p>
                        <span>Date de sortie : ${serie.first_air_date}</span>
                        ${serie.overview}
                        </p>
                    </div>
                </li>
              `
                    $tvShowsList.append(tpl);
                }
            }
        })
        //search in artist
        $.get(`https://api.themoviedb.org/3/search/person?api_key=4bf086d1df9acf40d8bb81636c65a8c8&language=fr&query=${$searchBar.val()}&page=1&include_adult=false`, function (response) {
            if (!response.results) {
                return;
            }
            for (const artist of response.results) {
                if (artist.known_for[2] == undefined) {
                    let tpl =
                        `
                <li id="${artist.id}" class="row">
                    <div class="filmTxt col-12">
                        <h3>${artist.name}</h3>
                    </div>
                </li>
              `
                    $artistList.append(tpl);
                }
                else {
                    let tpl =
                        `
                    <li id="${artist.id}" class="row">
                        <div class="filmTxt col-12">
                            <h3>${artist.name}</h3>
                            <p>
                                <span>A joué dans :</span>
                                ${artist.known_for[0].title}
                                <br/>
                                ${artist.known_for[1].title} 
                                <br/>
                                ${artist.known_for[2].title}     
                            </p>
                        </div>
                    </li>
                  `
                    $artistList.append(tpl);
                }
            }
        })
        $('#results').removeClass('hide');
        $searchBar.val('');
    });

    //show results in diferent categories
    $('#filmBtn').on('click', function () {
        $filmList.removeClass('hide');
        $tvShowsList.addClass('hide');
        $artistList.addClass('hide');
    });
    $('#tvShowBtn').on('click', function () {
        $filmList.addClass('hide');
        $tvShowsList.removeClass('hide');
        $artistList.addClass('hide');
    });
    $('#artistsBtn').on('click', function () {
        $filmList.addClass('hide');
        $tvShowsList.addClass('hide');
        $artistList.removeClass('hide');
    });

    //listen to clicks on film list to open a reuest on the movie with its id
    $filmList.on('click', 'a', function (e) {
        //don'topen the link
        e.preventDefault();
        //make a request on the movie
        $.get(`https://api.themoviedb.org/3/movie/${$(this)[0].id}?api_key=4bf086d1df9acf40d8bb81636c65a8c8&language=fr`, function (response) {
            if (!response) {
                return;
            }
            //emptying old results
            $filmList.html('');
            $tvShowsList.html('');
            $artistList.html('');
            //create a new template for the movie
            let tpl =
                `
                <li class="row">
                    <div class="col-3">
                        <img class="filmPict" src="https://image.tmdb.org/t/p/w500/${response.poster_path}">
                    </div>
                    <div class="filmTxt col-11 col-md-8">
                        <h3>${response.title}</h3>
                        <p>
                        ${response.overview}
                        <br/>
                        Production : ${response.production_companies[0].name}
                        <br/>
                        Budget : ${response.budget} $
                        <br/>
                        Recette : ${response.revenue} $
                        <br/>
                        Durée : ${Math.floor(response.runtime / 60)} h ${response.runtime % 60}
                        </p>
                    </div>
                </li>
              `
            //add the template to the movie list
            $filmList.append(tpl);

        })
    })
    // get more details on the tv show
    $tvShowsList.on('click', 'a', function (e) {
        e.preventDefault();
        $.get(`https://api.themoviedb.org/3/tv/${$(this)[0].id}?api_key=4bf086d1df9acf40d8bb81636c65a8c8&language=fr`, function (response) {
            if (!response) {
                return;
            }
            //emptying old results
            $filmList.html('');
            $tvShowsList.html('');
            $artistList.html('');
            //create a templte for the TV show
            let tpl = `
            <li id="${response.id}" class="row">
                <div class="col-3">
                        <img class="filmPict" src="https://image.tmdb.org/t/p/w500${response.poster_path}">
                </div>
                <div class="col-9">
                    <h3>
                        ${response.name}
                    </h3>
                    <p>
                    <span>Première diffusion : ${response.first_air_date}</span>
                    <br/>
                    <span>Nombre de saisons :${response.number_of_seasons}</span>
                    <br/>
                    <span>Nombre d'épisodes :${response.number_of_episodes}</span>
                    <br/>
                    <span>Genre : ${response.genres[0].name}</span>
                    <br/>
                    ${response.overview}               
                    </p>
                </div>
            </li>
            `
            $tvShowsList.append(tpl);
        })
    })

    //show top movies/Tv shows
    $('#topFilms').on('click', function () {
        $.get('https://api.themoviedb.org/3/movie/top_rated?api_key=4bf086d1df9acf40d8bb81636c65a8c8&language=fr&page=1', function (response) {
            $filmList.html('');
            $tvShowsList.html('');
            $artistList.html('');
            $topList.html('');
            if (!response.results) {
                return;
            }
            for (const movie of response.results) {
                if (!movie.poster_path) {
                    let tpl =
                        `
                <li class="row">
                    <div class="filmTxt col-12">
                        <a id="${movie.id}" href='' >${movie.title} + d'infos</a>
                        <p>
                        <span>Date de sortie : ${movie.first_air_date}</span>
                        ${(movie.overview).slice(0, 200)} ...
                        </p>
                    </div>
                </li>
              `
                    $topList.append(tpl);
                }
                else {
                    let tpl =
                        `
                <li class="row">
                    <div class="col-3">
                        <img class="filmPict" src="https://image.tmdb.org/t/p/w500/${movie.poster_path}">
                    </div>
                    <div class="filmTxt col-11 col-md-9">
                        <a id="${movie.id}" href='' >${movie.title} + d'infos</a>
                        <p>
                        <span>Date de sortie : ${movie.release_date}</span>
                        ${(movie.overview).slice(0, 200)} ...
                        </p>
                    </div>
                </li>
              `
                    $topList.append(tpl);
                }
            }
        })
        $('#top').removeClass('hide');
        //plus d'infos au clic sur le titre du film
        $topList.on('click', 'a', function (e) {
            //don'topen the link
            e.preventDefault();
            //make a request on the movie
            $.get(`https://api.themoviedb.org/3/movie/${$(this)[0].id}?api_key=4bf086d1df9acf40d8bb81636c65a8c8&language=fr`, function (response) {
                if (!response) {
                    return;
                }
                //emptying old results
                $topList.html('');
                //create a new template for the movie
                let tpl =
                    `
                    <li class="row">
                        <div class="col-3">
                            <img class="filmPict" src="https://image.tmdb.org/t/p/w500/${response.poster_path}">
                        </div>
                        <div class="filmTxt col-11 col-md-8">
                            <h3 href='' >${response.title}</h3>
                            <p>
                            ${response.overview}
                            <br/>
                            Production : ${response.production_companies[0].name}
                            <br/>
                            Budget : ${response.budget} $
                            <br/>
                            Recette : ${response.revenue} $
                            <br/>
                            Durée : ${Math.floor(response.runtime / 60)} h ${response.runtime % 60}
                            </p>
                        </div>
                        </li>
                  `
                //add the template to the movie list
                $topList.append(tpl);
            })
        })
    })
    

    //more information about the TV show
    $('#topTv').on('click', function (e) {
        $.get('https://api.themoviedb.org/3/tv/top_rated?api_key=4bf086d1df9acf40d8bb81636c65a8c8&language=fr&page=1', function (response) {
            $filmList.html('');
            $tvShowsList.html('');
            $artistList.html('');
            $topList.html('');
            if (!response.results) {
                return;
            }
            for (const serie of response.results) {
                if (!serie.poster_path) {
                    let tpl =
                        `
                <li id="${serie.id}" class="row">
                    <div class="filmTxt col-12">
                        <h3>${serie.name}</h3>
                        <p>
                        <span>Date de sortie : ${serie.first_air_date}</span>
                        ${serie.overview}
                        </p>
                    </div>
                </li>
              `
                    $topList.append(tpl);
                }
                else {
                    let tpl =
                        `
                <li class="row">
                    <div class="col-3">
                        <img class="filmPict" src="https://image.tmdb.org/t/p/w500/${serie.poster_path}">
                    </div>
                    <div class="filmTxt col-11 col-md-9">
                        <a id="${serie.id}" href='' >${serie.name} + d'infos</a>
                        <p>
                        <span>Date de sortie : ${serie.first_air_date}</span>
                        ${serie.overview}
                        </p>
                    </div>
                </li>
              `
                    $topList.append(tpl);
                }
            }
        })
        $('#top').removeClass('hide');
        $topList.on('click', 'a', function (e) {
            //don't open the link
            e.preventDefault();
            let tvId = $(this)[0].id;
            console.log(tvId);
            
            //make a request on the movie
            // $.get(`https://api.themoviedb.org/3/movie/${tvId}?api_key=4bf086d1df9acf40d8bb81636c65a8c8&language=fr`, function (response) {
            //     if (!response) {
            //         return;
            //     }
            //     //emptying old results
            //     $topList.html('');
            //     //create a new template for the movie
            //     let tpl = `
            //     <li id="${response.id}" class="row">
            //         <div class="col-3">
            //                 <img class="filmPict" src="https://image.tmdb.org/t/p/w500${response.poster_path}">
            //         </div>
            //         <div class="col-9">
            //             <h3>
            //                 ${response.name}
            //             </h3>
            //             <p>
            //             <span>Première diffusion : ${response.first_air_date}</span>
            //             <br/>
            //             <span>Nombre de saisons : ${response.number_of_seasons}</span>
            //             <br/>
            //             <span>Nombre d'épisodes : ${response.number_of_episodes}</span>
            //             <br/>
            //             <span>Genre : ${response.genres[0].name}</span>
            //             <br/>
            //             ${response.overview}               
            //             </p>
            //         </div>
            //     </li>
            // `
            //     //add the template to the movie list
            //     $topList.append(tpl);
            // })
        })
    })
});
